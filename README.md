# Onboarding - Frontend

This project helps the Frontend onboard its new members.

An [issue template](.gitlab/issue_templates/frontend_onboarding.md) is used to create onboarding issues which supplement the [general onboarding](https://gitlab.com/gitlab-com/people-ops/employment/issues) issues. 